//Setup
var express = require("express");
var mustacheExpress = require('mustache-express');
var app = express();

//Config
app.use('/public', express.static(__dirname + '/public'));
app.engine('html', mustacheExpress());
app.set('view engine', 'html');
app.set('views', __dirname + '/views');

//Database/Models
var Model = require('./model');
var User = Model.User;
var Location = Model.Location;

//Routes
require('./routes/home')(app);
require('./routes/user')(app, User);
require('./routes/events')(app, User);

//go
app.listen(process.env.PORT || 3000);
console.log('Server running...');
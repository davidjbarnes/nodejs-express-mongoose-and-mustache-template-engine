//User Routes

module.exports = function(app, User){
    app.get('/', function(req, res) {
        res.render('master', {
                main: {
                      title: 'Welcome'
                },
                content: {
                      header: 'Welcome'
                } 
        });
    });
    app.get('/user/select/:firstName', function(req, res) {
        var firstName = req.params.firstName;
        User.where({firstName:firstName}).findOne(function (err, u){
            res.render('master', {
                main: {
                      title: 'Select User'
                },
                content: {
                      header: "Name: " + u.firstName
                } 
            });
        });
    });
    app.get('/user/save/:firstName', function(req, res) {
        var firstName = req.params.firstName;
        var user = new User({ firstName: firstName, lastName: 'Thomas', enabled: true });
        user.save(function (err) {
            res.render('master', {
                main: {
                      title: 'Save User'
                },
                content: {
                    header: 'Save User'
                } 
            });
        });
    });
}
//Event Routes

module.exports = function(app, User){
    app.get('/events/:eventName', function(req, res) {
        var eventName = req.params.eventName;

        res.writeHead(200, {
            'Content-Type': 'text/event-stream',
            'Cache-Control': 'no-cache',
            'Connection': 'keep-alive'
        });

        if(eventName == "getLatestUser"){
            User.findOne({}, {}, { sort: { '_id' : -1 } }, function (err, u){
                res.write('data:{\'date\':\'' + Date() + '\', \'firstName\':\'' + u.firstName + '\'}\n\n');
                res.end();
            });
        }
        //Add more events here
    });
}
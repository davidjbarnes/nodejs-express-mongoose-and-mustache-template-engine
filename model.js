//Setup
var mongoose = require('mongoose');
var Schema = mongoose.Schema, ObjectId = Schema.ObjectId;

mongoose.connect('mongodb://localhost/test');

//Schemas
var UserSchema = new Schema({
    firstName: String, 
    lastName: String, 
    enabled: Boolean
});
var LocationSchema = new Schema({
    name: String,
    lat: String, 
    lng: String
});

//Models
mongoose.model('User', UserSchema);
mongoose.model('Location', LocationSchema);

//Exports
module.exports.User = mongoose.model('User');
module.exports.Location = mongoose.model('Location');